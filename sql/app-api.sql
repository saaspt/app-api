/*
 Navicat Premium Data Transfer

 Source Server         : 181
 Source Server Type    : MySQL
 Source Server Version : 50631
 Source Host           : 192.168.1.181:3306
 Source Schema         : app-api

 Target Server Type    : MySQL
 Target Server Version : 50631
 File Encoding         : 65001

 Date: 09/04/2021 21:54:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info`  (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '租户ID',
  `tenant_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '租户名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tenant_info
-- ----------------------------
INSERT INTO `tenant_info` VALUES ('8fc7877c-1f46-4a9a-b48e-18526b3f9a8a', '西安中服软件有限公司');

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`  (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `tenantid` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '租户ID',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '登录名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('4a50bc1c-3af9-4f2a-b2c2-3fc36a304c0d', '8fc7877c-1f46-4a9a-b48e-18526b3f9a8a', '940906763@qq.com');
INSERT INTO `user_info` VALUES ('65b6f53a-0b90-44ae-9025-d36b4ba2ab23', '8fc7877c-1f46-4a9a-b48e-18526b3f9a8a', 'app-api@qq.com');

SET FOREIGN_KEY_CHECKS = 1;
