package com.cserver.appapi.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tenant_info", schema = "app-api", catalog = "")
public class TenantInfo {

    private String id;
    private String tenantName;

    @Id
    @Column(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "tenant_name")
    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TenantInfo that = (TenantInfo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(tenantName, that.tenantName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tenantName);
    }
}
