package com.cserver.appapi.service;

import com.cserver.appapi.dao.TenantInfoDao;
import com.cserver.appapi.dao.UserInfoDao;
import com.cserver.appapi.model.TenantInfo;
import com.cserver.appapi.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class TenantInfoService {

    @Autowired
    private TenantInfoDao tenantInfoDao;

    @Autowired
    private UserInfoDao userInfoDao;

    public TenantInfo getTenantInfoById(String tenantid){

        return tenantInfoDao.getById(tenantid);
    }

    //创建租户
    public void addTenantInfo(TenantInfo tenantInfo, UserInfo userInfo){
        //生成租户
        tenantInfoDao.saveAndFlush(tenantInfo);
        //生成租户初始用户
        userInfoDao.saveAndFlush(userInfo);
    }


}
