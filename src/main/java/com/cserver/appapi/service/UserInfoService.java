package com.cserver.appapi.service;

import com.cserver.appapi.dao.UserInfoDao;
import com.cserver.appapi.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserInfoService {

    @Autowired
    private UserInfoDao userInfoDao;


   public UserInfo getUserInfoByUsername(String username){

       return userInfoDao.getByUsername(username);
   }

   public void addUserInfo(UserInfo userInfo){

       userInfoDao.saveAndFlush(userInfo);
   }

   public Integer countByTenantid(String id){

       return userInfoDao.countByTenantid(id);
   }

}
