package com.cserver.appapi.dao;

import com.cserver.appapi.model.TenantInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TenantInfoDao extends JpaRepository<TenantInfo,String> {

    public TenantInfo getById(String id);

}
