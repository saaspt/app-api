package com.cserver.appapi.dao;

import com.cserver.appapi.model.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoDao extends JpaRepository<UserInfo,String> {

    public UserInfo getByUsername(String username);

    public Integer countByTenantid(String tenantid);
}
